from scripts.core.function.calculate import calculation
from scripts.core.function.mail import send_test_mail
import schedule

import time
def send_gmail():
    send_test_mail('vignesh.ravishankar@knowledgelens.com')
    print("Success")

if __name__ == '__main__':
    schedule.every().day.at("03:00").do(send_gmail)
    schedule.every(1).minutes.do(send_gmail)

    while(True):
        schedule.run_pending()
        time.sleep(1)
